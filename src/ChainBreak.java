import java.util.ArrayList;
import java.util.List;
import java.lang.Integer;

/**
 * Created by Cranefly on 10/23/16.
 * @author Cranefly
 */
public class ChainBreak {

    /**
     * Calculates the chain break of a given list of integers.
     * @param a
     * @return
     */
    public static double cb(List<Integer> a){

        double k = 0;

        for(int i = a.size()-1; i >= 0; i--){

            if(i == a.size()-1){
                k = a.get(i);
            }else{
                k = a.get(i)+1/k;
            }

        }

        return k;
    }

    /**
     * Calculates the chain break for one int, building
     * the set of the natural numbers until the given int.
     * @param a
     * @return
     */
    public static double cb(int a){

        double k = 0;

        for(int i = a; i > 0; i--){

            if(i == a){
                k = i;
            }else{
                k = i+1/k;
            }
        }

        return k;
    }


    public static int cp(int p){

        int pm2 = 0;
        int pm1 = 1;



        return p;
    }

    public static void main (String args[]){

        List<Integer> a = new ArrayList<Integer>();

        int b = 5;

        for(int i = 1; i <= b; i++){
            a.add(i);
        }

        // Call with List
        double r = cb(a);

        // Call with int
        double s = cb(b);

        System.out.println(r);
        System.out.println(s);

    }

}
