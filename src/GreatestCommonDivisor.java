/**
 * Created by Cranefly on 10/22/16.
 * @author Cranefly
 */
public class GreatestCommonDivisor {

    /**
     * Implements the euclidean algorithm to find the greatest common divisor (gcd) of p and q.
     * @param p
     * @param q
     * @return gcd of p and q
     */
    public static int gcd(int p, int q){

        if(q == 0){
            return p;
        }

        return gcd(q, p % q);
    }

    public static void main (String args[]){

        assert (gcd(4,16) == 4);
        assert (gcd(16,4) == 4);
        assert (gcd(15,60) == 15);
        assert (gcd(15,65) == 5);
        assert (gcd(1052,52) == 4);

    }

}
